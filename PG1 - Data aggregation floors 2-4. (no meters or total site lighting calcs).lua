--********************************************************************************************
--Description: This program calulates the runtime of the lighting circuits on a zone by zone basis and stores the
--result in AV's 9-11. It also calculates the lighting consumption on a zone by zone basis and stores the results in AV6-11. --following this it performs another summation of the previous results and stores the total lighting consumption in AV70 to be --summed by the floor 1 MPM along with the other floors totals in the site and sent to EO. Following this  
--it calculates the runtime of the airhandlers for the individual room controllers. It performs the calculations
--for the lighting/airhandler every 5 minutes. 
--Version: 0.2
--Last Modification: May-19-2014 
--Author: ASF
--Comments: 
--********************************************************************************************


function main ()
  
  print("entering main function")
  
    --Initialize variables
    if PG3_init == nil then
      LastRunAirHandler = {}
      AirHandlerRuntime = {}
      AirHandlerRuntime[1] = {reference =  ME.AV12_Present_Value}
      AirHandlerRuntime[2] = {reference =  ME.AV13_Present_Value}
      AirHandlerRuntime[3] = {reference =  ME.AV14_Present_Value}
      AirHandlerRuntime[4] = {reference =  ME.AV15_Present_Value}
      AirHandlerRuntime[5] = {reference =  ME.AV16_Present_Value}
      AirHandlerRuntime[6] = {reference =  ME.AV17_Present_Value}
      LastRunZone1Lights = os.time() --create reference time on init for Zone 1 runtime calculation
      LastRunZone2Lights = os.time() --create reference time on init for Zone 2 runtime calculation
      LastRunZone3Lights = os.time() --create reference time on init for Zone 3 runtime calculation
      Lighting_Period = 300
      AirHandler_Period = 300
      var("Zone1Demand" , "ME.AV2")
      var("Zone2Demand" , "ME.AV3")
      var("Zone3Demand" , "ME.AV4")
      var("Zone1Total" , "ME.AV6")
      var("Zone2Total" , "ME.AV7")
      var("Zone3Total" , "ME.AV8")
      var("Zone1Runtime" , "ME.AV9")
      var("Zone2Runtime" , "ME.AV10")
      var("Zone3Runtime" , "ME.AV11")
      var("LightsTotalFloor" , "ME.AV70")
    
      for i = 1, 6 do --create reference time on init for all airhandlers runtime calculation
        table.insert( LastRunAirHandler, {currentTime = os.time()} )
      end
      
      AirHandler_objects = GetObjectsByName("Ah0.Ena") -- find Ah01-06Ena but ignore AhxxRt
    end --init complete
    PG3_init = true

    -----------------------------------
    --main program
    -----------------------------------
    if every(Lighting_Period) then --calculate lighting consumption by multiplying instantaneous demand by runtime
      LightsTotalFloor = 0 -- reset total as inidvidual zones are cumulative
      Zone1Total = FloorLightingConsumption("SCH1_Present_Value",LastRunZone1Lights, Zone1Total,Zone1Runtime, Zone1Demand )
      Zone2Total = FloorLightingConsumption("SCH2_Present_Value",LastRunZone2Lights, Zone2Total,Zone2Runtime, Zone3Demand)
      Zone3Total = FloorLightingConsumption("SCH3_Present_Value",LastRunZone3Lights, Zone3Total,Zone2Runtime, Zone3Demand)
      LightsTotalFloor = Zone1Total + Zone2Total + Zone3Total
    end
    
    if every(AirHandler_Period) then --calculate Airhandler runtime
      Airhandler_Runtime()
    end 
end -- end of main program



-----------------------------------
--this function splits a given string based on a seperator
-----------------------------------
function string:split(sep)
        local sep, fields = sep or ":", {}
        local pattern = string.format("([^%s]+)", sep)
        self:gsub(pattern, function(c) fields[#fields+1] = c end)
        return fields
end

-----------------------------------
-- This function returns a table of objects that match a given name
-----------------------------------
function GetObjectsByName(NameToMatch)
  c2objects = {}  
  for i, dev in ipairs(GetDevices("%w*")) do
    for i, c2object in ipairs( ME[dev.id .. "_Object_Pool"].value:split(",") ) do
      if ME[c2object .. "_Object_Name"] then
        if ME[c2object .. "_Object_Name"].value then 
          field = string.match(ME[c2object .. "_Object_Name"].value, NameToMatch) 
          if field then              
            table.insert(c2objects, { id=c2object, name = ME[c2object .. "_Object_Name" ], reference = ME[c2object .. "_Present_Value"]} )
          end
        end
      end
    end
  end
  return c2objects
end

-----------------------------------
--this function searches through the controller dev list and returns a table of valid alphanumeric devices with id and name
-----------------------------------
function GetDevices(deviceType)
  devices = {}
  for i, device in ipairs( ME["CFG1_DEV_List"].value:split(",") ) do
    if string.find(device,deviceType) then
      devid=device:split(":")[1]
      table.insert(devices, { id=devid, name = ME[devid .. "_Object_Name" ].value } )      
    end
  end
  return devices
end

-----------------------------------
--this function calculates Lighting consumption for each zone, sums the result and stores it in AV70
-----------------------------------
function FloorLightingConsumption (Schedule, LastRunZoneLights, ZoneTotal, ZoneRunTime, ZoneDemand)
  
  if ME[Schedule].value == 0 then
    LastRunZoneLights = os.time()
  elseif ME[Schedule].value == 1 then
    RunTimeZoneLights = runtime(LastRunZoneLights)
    LastRunZoneLights = os.time()
    ZoneRunTime = ZoneRunTime + RunTimeZoneLights
    ZoneTotal = ZoneTotal + (RunTimeZoneLights * ZoneDemand * 1000)/3600
  end
  return ZoneTotal
end


----------------------------------
--this function handles Airhandler runtime calculation
----------------------------------

function Airhandler_Runtime ()
   
  for i , obj in ipairs(AirHandler_objects) do
    if obj.reference.value == 0 then -- if AirHandler is off then move last run up 5 minutes 
      LastRunAirHandler[i].currentTime = os.time()  
    elseif obj.reference.value == 1 then -- if AirHandler is on then calculate exact run time in last 5 minutes and add to the total
      RunTimeAirHandler = runtime(LastRunAirHandler[i].currentTime)
      LastRunAirHandler[i].currentTime = os.time()
      AirHandlerRuntime[i].reference.value = AirHandlerRuntime[i].reference.value + RunTimeAirHandler
    end
    print(AirHandlerRuntime[i].reference.value)
  end
 
end  
----------------------------------
--this function handles runtime calculation
----------------------------------

function runtime (lastrun)
  currentrun = os.time()
  time_diff = currentrun - lastrun
  return time_diff
end  
  
  
  
  
main()